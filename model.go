package main

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"


)

// ConnectionState is the type used to track the state of our connection
type ConnectionState int

// ConnectionState constants
const (
	ConnectionStateConnecting ConnectionState = iota
	ConnectionStateProcessing
	ConnectionStateDisconnecting
)

// String method for ConnectionState
func (s ConnectionState) String() string {
	switch s {
	case ConnectionStateConnecting:
		return "connecting"
	case ConnectionStateProcessing:
		return "processing"
	case ConnectionStateDisconnecting:
		return "disconnecting"
	}
	return "invalid"
}

// Frame decribes a SPOP frame
type Frame struct {
	Type     FrameType
	Metadata Metadata
	Payload  interface{}
}


type Metadata struct {
	FrameFlags Flags
	StreamID   uint64
	FrameID    uint64
}

// FlagBits type
type FlagBits uint32

// FlagBits constants
const (
	FlagFin FlagBits = 1 << iota
	FlagAbort
)

// Flags defines the two bit flags
type Flags struct {
	Fin   bool
	Abort bool
}

// FrameType is the type used for each frame
type FrameType uint8


// FrameType constants
const (
	FrameTypeUnset FrameType = iota
	FrameTypeHAProxyHello
	FrameTypeHAProxyDisconnect
	FrameTypeNotify
	FrameTypeAgentHello
	FrameTypeAgentDisconnect
	FrameTypeAck
)

// Byte method for FrameType
func (t FrameType) Byte() byte {
	return byte(t)
}

// String method for FrameType
func (t FrameType) String() string {
	switch t {
	case FrameTypeUnset:
		return "UNSET"
	case FrameTypeHAProxyHello:
		return "HAPROXY-HELLO"
	case FrameTypeHAProxyDisconnect:
		return "HAPROXY-DISCONNECT"
	case FrameTypeNotify:
		return "NOTIFY"
	case FrameTypeAgentHello:
		return "AGENT-HELLO"
	case FrameTypeAgentDisconnect:
		return "AGENT-DISCONNECT"
	case FrameTypeAck:
		return "ACK"
	}

	return "UNKNOWN"
}

// ErrorCode is the error code type in SPOP
type ErrorCode uint8

// ErrorCode constant list
const (
	ErrorCodeNone ErrorCode = iota
	ErrorCodeIO
	ErrorCodeTimeout
	ErrorCodeTooBig
	ErrorCodeInvalid
	ErrorCodeMissingVersion
	ErrorCodeMissingMaxSize
	ErrorCodeMissingCapabilities
	ErrorCodeUnsupportedVersion
	ErrorCodeUnsupportedSize
	ErrorCodeFragmentationUnsupported
	ErrorCodeInterlaceInvalid
	ErrorCodeMissingFrameID
	ErrorCodeResourceAllocationError
	ErrorCodeUnknown ErrorCode = 99
)

// String method for ErrorCode
func (e ErrorCode) String() (s string) {
	switch e {
	case ErrorCodeNone:
		s = "normal (no error occurred)"
	case ErrorCodeIO:
		s = "I/O error"
	case ErrorCodeTimeout:
		s = "A timeout occurred"
	case ErrorCodeTooBig:
		s = "frame is too big"
	case ErrorCodeInvalid:
		s = "invalid frame received"
	case ErrorCodeMissingVersion:
		s = "version value not found"
	case ErrorCodeMissingMaxSize:
		s = "max-frame-size value not found"
	case ErrorCodeMissingCapabilities:
		s = "capabilities value not found"
	case ErrorCodeUnsupportedVersion:
		s = "unsupported version"
	case ErrorCodeUnsupportedSize:
		s = "max-frame-size too big or too small"
	case ErrorCodeFragmentationUnsupported:
		s = "payload fragmentation is not supported"
	case ErrorCodeInterlaceInvalid:
		s = "invalid interlaced frames"
	case ErrorCodeMissingFrameID:
		s = "frame-id not found (it does not match any referenced frame)"
	case ErrorCodeResourceAllocationError:
		s = "resource allocation error"
	case ErrorCodeUnknown:
		s = "an unknown error occurred"
	default:
		s = strconv.Itoa(int(e))
	}

	return s
}

// CapabilityString is a slice of bytes
type CapabilityString []byte

// CapabilityList lists frame capabilities
type CapabilityList struct {
	Fragmentation bool `spop:"fragmentation"`
	Pipelining    bool `spop:"pipelining"`
	Async         bool `spop:"async"`
}

// String method for CapabilityString
func (s CapabilityString) String() string {
	return fmt.Sprintf("%s", string(s))
}

// Marshal encodes a CapabilityList into a CapabilityString
func (l CapabilityList) Marshal() (s CapabilityString) {
	var capabilities []string

	v := reflect.ValueOf(l)
	t := reflect.TypeOf(l)

	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Bool() {
			capabilities = append(capabilities, t.Field(i).Tag.Get("spop"))
		}
	}

	return CapabilityString(strings.Join(capabilities, ","))
}

// Unmarshal decodes a CapabilityString into a CapabilityList
func (s CapabilityString) Unmarshal() (l CapabilityList) {

	capabilities := strings.Split(s.String(), ",")

	// TODO: Use reflect for this rather than hard coding
	for i := 0; i < len(capabilities); i++ {
		switch capabilities[i] {
		case "fragmentation":
			l.Fragmentation = true
		case "pipelining":
			l.Pipelining = true
		case "async":
			l.Async = true
		}
	}

	return l
}

// PayloadHAProxyHello is the supported payload for a HAPROXY-HELLO message
type PayloadHAProxyHello struct {
	SupportedVersions string         `spop:"supported-versions"`
	MaxFrameSize      uint32         `spop:"max-frame-size"`
	Capabilities      CapabilityList `spop:"capabilities"`
	Healthcheck       bool           `spop:"healthcheck"`
	EngineID          string         `spop:"engine-id"`
}

// PayloadDisconnect is the supported payload for a <HAPROXY|AGENT>-DISCONNECT frame
type PayloadDisconnect struct {
	StatusCode ErrorCode `spop:"status-code"`
	Message    string    `spop:"message"`
}

// PayloadAgentHello is the supported payload for a AGENT-HELLO frame
type PayloadAgentHello struct {
	Version      string         `spop:"version"`
	MaxFrameSize uint32         `spop:"max-frame-size"`
	Capabilities CapabilityList `spop:"capabilities"`
}

type VariableScope uint8

const (
	ScopeProcess VariableScope = iota
	ScopeSession
	ScopeTransaction
	ScopeRequest
	ScopeResponse
)

// String method for VariableScope
func (v VariableScope) String() string {
	switch v {
	case ScopeProcess:
		return "proc"
	case ScopeRequest:
		return "req"
	case ScopeResponse:
		return "res"
	case ScopeSession:
		return "sess"
	case ScopeTransaction:
		return "txn"
	}
	return "unknown"
}

// Byte method for VariableScope
func (v VariableScope) Byte() byte {
	return byte(v)
}

type ActionType uint8

const (
	ActionSetVar ActionType = iota + 1
	ActionUnsetVar
)

// Byte method for ActionType
func (a ActionType) Byte() byte {
	return byte(a)
}

// String method for ActionType
func (a ActionType) String() string {
	switch a {
		case ActionSetVar: return "ACTION-SET-VAR"
		case ActionUnsetVar: return "ACTION-UNSET-VAR"	
	}
	return ""
}

type AckVariable struct {
	Name  string
	Value interface{}
}

type UnsetVariable struct {
	Scope VariableScope
	Name  string
}

type PayloadAck struct {
	ActionList   map[VariableScope]map[string]interface{}
}

// DataType is the type for the various types of typed data used
type DataType uint8

// DataType constants
const (
	TypeNull DataType = iota
	TypeBool
	TypeInt32
	TypeUint32
	TypeInt64
	TypeUint64
	TypeIPv4
	TypeIPv6
	TypeString
	TypeBinary
)

// String method for DataType
func (t DataType) String() string {
	switch t {
	case TypeNull:
		return "NULL"
	case TypeBool:
		return "bool"
	case TypeInt32:
		return "int32"
	case TypeUint32:
		return "uint32"
	case TypeInt64:
		return "int64"
	case TypeUint64:
		return "uint64"
	case TypeIPv4:
		return "IPv4"
	case TypeIPv6:
		return "IPv6"
	case TypeString:
		return "string"
	case TypeBinary:
		return "binary"
	default:
		return "unused/reserved"
	}
}

// Unmarshal is the generic unmarshal method
func Unmarshal(data []byte, v interface{}) error {
	if p, ok := v.(*PayloadHAProxyHello); ok {
		return p.Unmarshal(data)
	}

	if p, ok := v.(*PayloadDisconnect); ok {
		return p.Unmarshal(data)
	}

	return fmt.Errorf("unhandled type in Unmarshal")
}

// Marshal is the generic marshal method
func Marshal(v interface{}) ([]byte, error) {
	if p, ok := v.(PayloadDisconnect); ok {
		return p.Marshal()
	}

	if p, ok := v.(PayloadAck); ok {
		return p.Marshal()
	}

	if p, ok := v.(PayloadAgentHello); ok {
		return p.Marshal()
	}

	if f, ok := v.(Frame); ok {
		return f.Marshal()
	}

	return nil, fmt.Errorf("unhandled type in Marshal()")
}
