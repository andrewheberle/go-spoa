package main

import (
	"reflect"
	"testing"
)

func TestCapabilityList_Marshal(t *testing.T) {
	tests := []struct {
		name  string
		l     CapabilityList
		wantS CapabilityString
	}{
		{"asynconly", CapabilityList{Fragmentation: false, Async: true, Pipelining: false}, CapabilityString("async")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotS := tt.l.Marshal(); !reflect.DeepEqual(gotS, tt.wantS) {
				t.Errorf("CapabilityList.Marshal() = %v, want %v", gotS, tt.wantS)
			}
		})
	}
}

func TestPayloadDisconnect_Marshal(t *testing.T) {
	tests := []struct {
		name    string
		p       PayloadDisconnect
		want    []byte
		wantErr bool
	}{
		{"noerror", PayloadDisconnect{StatusCode: ErrorCodeNone, Message: "no error"}, []byte{11, 115, 116, 97, 116, 117, 115, 45, 99, 111, 100, 101, 0, 7, 109, 101, 115, 115, 97, 103, 101, 8, 110, 111, 32, 101, 114, 114, 111, 114}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.p.Marshal()
			if (err != nil) != tt.wantErr {
				t.Errorf("PayloadDisconnect.Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PayloadDisconnect.Marshal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMarshal(t *testing.T) {
	type args struct {
		v interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{"agentdisconnect", args{PayloadDisconnect{StatusCode: ErrorCodeNone, Message: "no error"}}, []byte{11, 115, 116, 97, 116, 117, 115, 45, 99, 111, 100, 101, 0, 7, 109, 101, 115, 115, 97, 103, 101, 8, 110, 111, 32, 101, 114, 114, 111, 114}, false},
		{"unhandled", args{PayloadHAProxyHello{}}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Marshal(tt.args.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Marshal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFrameType_String(t *testing.T) {
	tests := []struct {
		name string
		t    FrameType
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.String(); got != tt.want {
				t.Errorf("FrameType.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestErrorCode_String(t *testing.T) {
	tests := []struct {
		name  string
		e     ErrorCode
		wantS string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotS := tt.e.String(); gotS != tt.wantS {
				t.Errorf("ErrorCode.String() = %v, want %v", gotS, tt.wantS)
			}
		})
	}
}

func TestCapabilityString_String(t *testing.T) {
	tests := []struct {
		name string
		s    CapabilityString
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.String(); got != tt.want {
				t.Errorf("CapabilityString.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestVariableScope_String(t *testing.T) {
	tests := []struct {
		name string
		s    VariableScope
		want string
	}{
		{"sess", ScopeSession, "sess"},
		{"proc", ScopeProcess, "proc"},
		{"req", ScopeRequest, "req"},
		{"res", ScopeResponse, "res"},
		{"txn", ScopeTransaction, "txn"},
		{"invalid", 10, "unknown"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.String(); got != tt.want {
				t.Errorf("VariableScope.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDataType_String(t *testing.T) {
	tests := []struct {
		name string
		t    DataType
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.String(); got != tt.want {
				t.Errorf("DataType.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPayloadAgentHello_Marshal(t *testing.T) {
	tests := []struct {
		name    string
		p       PayloadAgentHello
		want    []byte
		wantErr bool
	}{
		{"defaulthello", AgentHello, []byte{7, 118, 101, 114, 115, 105, 111, 110, 3, 50, 46, 48, 14, 109, 97, 120, 45, 102, 114, 97, 109, 101, 45, 115, 105, 122, 101, 252, 127, 0, 12, 99, 97, 112, 97, 98, 105, 108, 105, 116, 105, 101, 115, 0}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.p.Marshal()
			if (err != nil) != tt.wantErr {
				t.Errorf("PayloadAgentHello.Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PayloadAgentHello.Marshal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPayloadAck_Marshal(t *testing.T) {
	tests := []struct {
		name    string
		p       PayloadAck
		want    []byte
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.p.Marshal()
			if (err != nil) != tt.wantErr {
				t.Errorf("PayloadAck.Marshal() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PayloadAck.Marshal() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCapabilityString_Unmarshal(t *testing.T) {
	tests := []struct {
		name  string
		s     CapabilityString
		wantL CapabilityList
	}{
		{"asynconly", []byte("async"), CapabilityList{Async: true, Pipelining: false, Fragmentation: false}},
		{"fragmentationonly", []byte("fragmentation"), CapabilityList{Async: false, Pipelining: false, Fragmentation: true}},
		{"pipeliningonly", []byte("pipelining"), CapabilityList{Async: false, Pipelining: true, Fragmentation: false}},
		{"all", []byte("async,fragmentation,pipelining"), CapabilityList{Async: true, Pipelining: true, Fragmentation: true}},
		{"none", nil, CapabilityList{Async: false, Pipelining: false, Fragmentation: false}},
		{"afonly", []byte("async,fragmentation"), CapabilityList{Async: true, Pipelining: false, Fragmentation: true}},
		{"aponly", []byte("async,pipelining"), CapabilityList{Async: true, Pipelining: true, Fragmentation: false}},
		{"pfonly", []byte("pipelining,fragmentation"), CapabilityList{Async: false, Pipelining: true, Fragmentation: true}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotL := tt.s.Unmarshal(); !reflect.DeepEqual(gotL, tt.wantL) {
				t.Errorf("CapabilityString.Unmarshal() = %v, want %v", gotL, tt.wantL)
			}
		})
	}
}

func TestPayloadHAProxyHello_Unmarshal(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		p       *PayloadHAProxyHello
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.p.Unmarshal(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("PayloadHAProxyHello.Unmarshal() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUnmarshal(t *testing.T) {
	type args struct {
		data []byte
		v    interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Unmarshal(tt.args.data, tt.args.v); (err != nil) != tt.wantErr {
				t.Errorf("Unmarshal() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
