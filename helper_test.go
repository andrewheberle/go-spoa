package main

import "testing"

func TestUvarintSize(t *testing.T) {
	type args struct {
		v uint64
	}
	tests := []struct {
		name  string
		args  args
		wantN int
	}{
		{"onebytevarint", args{200}, 1},
		{"twobytevarint", args{2000}, 2},
		{"threebytevarint", args{200000}, 3},
		{"fourbytevarint", args{20000000}, 4},
		{"fivebytevarint", args{4000000000}, 5},
		{"toobigvarint", args{7000000000}, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotN := UvarintSize(tt.args.v); gotN != tt.wantN {
				t.Errorf("UvarintSize() = %v, want %v", gotN, tt.wantN)
			}
		})
	}
}
