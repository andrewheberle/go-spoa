package main

import (
	"fmt"
	"log"
	"reflect"

	"github.com/hashicorp/go-version"
)

// Unmarshal method for PayloadHAProxyHello
func (p *PayloadHAProxyHello) Unmarshal(data []byte) error {
	ppos := 0

	for {
		if ppos >= len(data) {
			break
		}

		v, n, err := DecodeString(data[ppos:])
		if err != nil {
			return fmt.Errorf("error decoding key name")
		}
		ppos = ppos + n

		switch v {
		case "supported-versions":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding supported-versions")
			}
			p.SupportedVersions = v.(string)
			ppos = ppos + n
		case "max-frame-size":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding max-frame-size")
			}
			p.MaxFrameSize = v.(uint32)
			ppos = ppos + n
		case "capabilities":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding capabilities")
			}
			p.Capabilities = CapabilityString(v.(string)).Unmarshal()
			ppos = ppos + n
		case "healthcheck":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding healthcheck")
			}
			p.Healthcheck = v.(bool)
			ppos = ppos + n
		case "engine-id":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding engine-id")
			}
			p.EngineID = v.(string)
			ppos = ppos + n
		default:
			log.Printf("skipped: %s", v)
		}
	}

	return nil
}

// Validate the PayloadHAProxyHello payload
func (p *PayloadHAProxyHello) Validate() error {
	// Check supported version
	hapVersion, err := version.NewVersion(p.SupportedVersions)
	if err != nil {
		return err
	}
	agentVersion, err := version.NewVersion(AgentHello.Version)
	if err != nil {
		return err
	}
	if !hapVersion.LessThanOrEqual(agentVersion) {
		return fmt.Errorf("unsupported version")
	}

	return nil
}

// Marshal outputs PayloadAgentHello as an encoded slice of bytes that is
// suitable to send over the wire as the payload of a frame
func (p PayloadAgentHello) Marshal() ([]byte, error) {
	// Build payload
	payload := make([]byte, 0)

	v := reflect.ValueOf(p)

	log.Print("Encoding AGENT-HELLO payload...")

	for i := 0; i < v.NumField(); i++ {
		kname := v.Type().Field(i).Tag.Get("spop")
		log.Printf("Doing key: %s", kname)
		buf, _, err := EncodeString(kname)
		if err != nil {
			return payload, err
		}
		payload = append(payload, buf...)
		switch (v.Field(i).Interface()).(type) {
		case string:
			log.Printf("Doing string value: %s", v.Field(i).String())
			buf, _, err = EncodeString(v.Field(i).String())
			if err != nil {
				return payload, err
			}
		case uint32:
			log.Printf("Doing uint32 value: %d", v.Field(i).Uint())
			buf, _ = EncodeUvarint(v.Field(i).Uint())
		case CapabilityList:
			log.Printf("Doing CapabilityList value: %s", string((v.Field(i).Interface()).(CapabilityList).Marshal()))
			buf, _, err = EncodeString(string((v.Field(i).Interface()).(CapabilityList).Marshal()))
			if err != nil {
				return payload, err
			}
		}
		payload = append(payload, buf...)
	}

	return payload, nil
}
