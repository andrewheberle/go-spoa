package main

import (
	"flag"
	"fmt"
	"log"
	"net"
)

func main() {
	var listenPort int
	var listenAddress string

	flag.StringVar(&listenAddress, "address", "", "Specify the address to listen on")
	flag.IntVar(&listenPort, "port", 5000, "Specify the port to listen on")
	flag.Parse()

	ln, err := net.Listen("tcp", fmt.Sprintf("%s:%d", listenAddress, listenPort))
	if err != nil {
		log.Fatalf("error listening: %s", err)
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("error accepting connection: %s", err)
		}
		go handleConnection(conn)
	}
}
