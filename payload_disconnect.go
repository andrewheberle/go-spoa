package main

import (
	"fmt"
	"log"
	"reflect"
)

// Marshal outputs PayloadDisconnect as an encoded slice of bytes that is
// suitable to send over the wire as the payload of a frame
func (p PayloadDisconnect) Marshal() ([]byte, error) {
	// Build payload
	payload := make([]byte, 0)

	v := reflect.ValueOf(p)

	for i := 0; i < v.NumField(); i++ {
		kname := v.Type().Field(i).Tag.Get("spop")
		buf, _, err := EncodeString(kname)
		if err != nil {
			return payload, err
		}
		payload = append(payload, buf...)
		switch v.Field(i).Kind() {
		case reflect.String:
			buf, _, err = EncodeString(v.Field(i).String())
			if err != nil {
				return payload, err
			}
		case reflect.Uint8:
			buf, _ = EncodeUvarint(v.Field(i).Uint())
		}
		payload = append(payload, buf...)
	}

	return payload, nil
}

// Unmarshal method for PayloadDisconnect
func (p *PayloadDisconnect) Unmarshal(data []byte) error {
	ppos := 0

	for {
		if ppos >= len(data) {
			break
		}

		v, n, err := DecodeString(data[ppos:])
		if err != nil {
			return fmt.Errorf("error decoding key name")
		}
		ppos = ppos + n

		switch v {
		case "status-code":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding status-code")
			}
			p.StatusCode = ErrorCode(v.(uint32))
			log.Printf("status-code = %s (%d)", p.StatusCode, p.StatusCode)
			ppos = ppos + n
		case "message":
			v, n, err := DecodeTypedData(data[ppos:])
			if err != nil {
				return fmt.Errorf("error decoding message")
			}
			p.Message = v.(string)
			log.Printf("message = %s", p.Message)
			ppos = ppos + n
		}
	}

	return nil
}
