package main

import (
	"encoding/binary"
	"fmt"
	"log"
)

// EncodeFrameType encodes a FrameType as a byte
func EncodeFrameType(t FrameType) (b byte) {
	return byte(t)
}

// Encode method for FrameType
func (t *FrameType) Encode() (b []byte, err error) {
	return []byte{byte(*t)}, nil
}

// EncodeMetadata encodes a Metadata as an array of bytes
func EncodeMetadata(m Metadata) (buf []byte, err error) {

	flags := make([]byte, 4)

	// Encode STREAM-ID
	streamID, n := EncodeUvarint(m.StreamID)
	if n == 0 {
		return nil, fmt.Errorf("error encoding stream-id")
	}

	// Encode FRAME-ID
	frameID, n := EncodeUvarint(m.FrameID)
	if n == 0 {
		return nil, fmt.Errorf("error encoding frame-id")
	}

	buf = append(buf, flags...)
	buf = append(buf, streamID...)
	buf = append(buf, frameID...)

	return buf, nil
}

// Encode method for Metadata
func (m *Metadata) Encode() (buf []byte, err error) {

	flags := make([]byte, 4)

	// Encode STREAM-ID
	streamID, n := EncodeUvarint(m.StreamID)
	if n == 0 {
		return nil, fmt.Errorf("error encoding stream-id")
	}

	// Encode FRAME-ID
	frameID, n := EncodeUvarint(m.FrameID)
	if n == 0 {
		return nil, fmt.Errorf("error encoding frame-id")
	}

	buf = append(buf, flags...)
	buf = append(buf, streamID...)
	buf = append(buf, frameID...)

	return buf, nil
}

// EncodeUvarint takes "v" and encodes it as a varint in a slice of bytes
func EncodeUvarint(v uint64) (buf []byte, n int) {
	buf = make([]byte, UvarintSize(v))
	n = binary.PutUvarint(buf, v)
	log.Printf("Encoded varint = %d; buf = %v", v, buf)
	return buf, n
}

// EncodeString takes "s" and encodes it as a slice of bytes
func EncodeString(s string) (buf []byte, n int, err error) {
	// encode length of s as a varint
	buf, n = EncodeUvarint(uint64(len(s)))
	if n <= 0 {
		return buf, n, fmt.Errorf("string length <= 0")
	}

	// append actual string
	buf = append(buf, []byte(s)...)

	return buf, len(buf), nil
}

// EncodeTypedData encodes the provided data as a slice of bytes
func EncodeTypedData(data interface{}) (buf []byte, n int, err error) {
	switch data.(type) {
	case string:
		buf, n, err = EncodeString(data.(string))
	case uint64:
		buf, n = EncodeUvarint(data.(uint64))
		err = nil
	default:
		err = fmt.Errorf("unsupported type")
	}
	return buf, n, err
}
