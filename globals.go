package main

// AgentHello is the default settings for an AGENT-HELLO frame
var AgentHello = PayloadAgentHello{
	Version:      "2.0",
	MaxFrameSize: 16380,
	Capabilities: CapabilityList{
		Fragmentation: false,
		Async:         false,
		Pipelining:    false,
	},
}
