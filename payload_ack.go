package main

// Marshal method for PayloadAck
func (p *PayloadAck) Marshal() ([]byte, error) {

	// Build payload
	payload := make([]byte, 0)

	// Add set actions to payload
	for scope, action := range p.ActionList {
		for name, value := range action {
			vname, _, err := EncodeString(name)
			if err != nil {
				return nil, err
			}
			if value != nil {
				vval, _, err := EncodeTypedData(value)
				if err != nil {
					return nil, err
				}
				payload = append(payload, []byte{(ActionSetVar).Byte(), 3, scope.Byte()}...)
				payload = append(payload, vname...)
				payload = append(payload, vval...)
			} else {
				payload = append(payload, []byte{(ActionUnsetVar).Byte(), 2, scope.Byte()}...)
				payload = append(payload, vname...)
			}
		}
	}

	return payload, nil
}

// AddSetAction adds an action to set the variable "n" in the scope "s" with
// the value "v"
func (p *PayloadAck) AddSetAction(s VariableScope, n string, v string) {
	p.ActionList[s][n] = v
}

// AddUnsetAction adds an action to unset the variable "n" in the scope "s"
func (p *PayloadAck) AddUnsetAction(s VariableScope, n string) {
	p.ActionList[s][n] = nil
}

// RemoveAction removes an action to set/unset the variable "n" in the scope
// "s"
func (p *PayloadAck) RemoveAction(s VariableScope, n string, v string) {
	delete(p.ActionList[s], n)
}
