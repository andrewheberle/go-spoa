package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
)

func DecodeFrameType(b byte) (t FrameType) {
	return FrameType(b)
}

func DecodeMetadata(buf []byte) (m Metadata, s int, err error) {
	var n int

	s = 4

	m.FrameFlags, err = DecodeFlags(buf[0:s])
	if err != nil {
		return m, s, err
	}

	// Get STREAM-ID
	m.StreamID, n, err = DecodeUvarint(buf[s:])
	if err != nil {
		return m, s, err
	}

	// move along n bytes
	s = s + n

	// Get FRAME-ID
	m.FrameID, n, err = DecodeUvarint(buf[s:])
	if err != nil {
		return m, s, err
	}

	// move along n bytes
	s = s + n

	return m, s, nil
}

func DecodeFlags(buf []byte) (f Flags, err error) {
	var flags FlagBits

	log.Printf("decoding flags. length = %d", len(buf))

	flagbuf := bytes.NewReader(buf)
	err = binary.Read(flagbuf, binary.BigEndian, &flags)
	if err != nil {
		log.Printf("problem with reading flags: %s", err)
		return Flags{}, err
	}
	for i, flag := range []FlagBits{FlagFin, FlagAbort} {
		val := flags&flag != 0
		log.Printf("flag bit: %d; value: %t", i, val)
		// TODO: use reflection here
		switch i {
		case 0:
			f.Fin = val
		case 1:
			f.Abort = val
		}
	}
	return f, nil
}

// DecodeUvarint is provided with a slice of bytes and decodes a Uvarint
// returning the int (uint64), the number of bytes read (n) and and error
// (err) variable
func DecodeUvarint(buf []byte) (v uint64, n int, err error) {

	v, n = binary.Uvarint(buf)
	if n == 0 {
		return v, n, fmt.Errorf("buf too small")
	}

	if n < 0 {
		return v, n, fmt.Errorf("overflow")
	}

	return v, n, nil

}

func UvarintSize(v uint64) (n int) {
	switch {
	case v < 240:
		return 1
	case v < 2288:
		return 2
	case v < 264432:
		return 3
	case v < 33818864:
		return 4
	case v < 4328786160:
		return 5
	}
	return 0
}

func DecodeVarint(buf []byte) (v int64, n int, err error) {

	v, n = binary.Varint(buf)
	if n == 0 {
		return v, n, fmt.Errorf("buf too small")
	}

	if n < 0 {
		return v, n, fmt.Errorf("overflow")
	}

	return v, n, nil

}

func DecodeTypedDataHeader(b byte) (t DataType, f bool) {
	// Grab second "nibble" as type
	t = DataType(b & 0x0F)

	// First bit of first "nibble" as flags
	flags := int((t & 0xF0) >> 7)

	if t == TypeBool {
		f = (flags == 1)
	}

	return t, f
}

func DecodeTypedData(buf []byte) (r interface{}, n int, err error) {
	var pos int

	t, f := DecodeTypedDataHeader(buf[pos])
	log.Printf("typed data: type = %s; flags = %t; bytes = 1", t, f)
	pos++
	switch t {
	case TypeString:
		s, n, err := DecodeString(buf[pos:])
		if err != nil {
			return nil, pos, err
		}
		log.Printf("decoded string: value = %s; bytes = %d", s, n)
		pos = pos + n
		r = s
	case TypeInt32:
		i, n, err := DecodeInt32(buf[pos:])
		if err != nil {
			return nil, pos, err
		}
		log.Printf("decoded int32: value = %d; bytes = %d", i, n)
		pos = pos + n
		r = i
	case TypeInt64:
		i, n, err := DecodeInt64(buf[pos:])
		if err != nil {
			return nil, pos, err
		}
		log.Printf("decoded int64: value = %d; bytes = %d", i, n)
		pos = pos + n
		r = i
	case TypeUint32:
		i, n, err := DecodeUint32(buf[pos:])
		if err != nil {
			return nil, pos, err
		}
		log.Printf("decoded uint32: value = %d; bytes = %d", i, n)
		pos = pos + n
		r = i
	case TypeUint64:
		i, n, err := DecodeUint64(buf[pos:])
		if err != nil {
			return nil, pos, err
		}
		log.Printf("decoded uint64: value = %d; bytes = %d", i, n)
		pos = pos + n
		r = i
	case TypeBool:
		log.Printf("decoded bool: value = %t", f)
		r = f
	}
	return r, pos, nil
}

// DecodeString is passed a slice of bytes (buf) and decodes
func DecodeString(buf []byte) (s string, n int, err error) {
	// Get string length
	length, n, err := DecodeUvarint(buf)
	if err != nil {
		return s, n, err
	}

	// Check its not too long
	if n+int(length) > len(buf) {
		return s, n, fmt.Errorf("decoded length too big: %d", length)
	}

	// Return string plus bytes read with nil error
	return string(buf[n : n+int(length)]), n + int(length), nil
}

func DecodeInt32(buf []byte) (i int32, n int, err error) {
	v, n, err := DecodeVarint(buf)
	if err != nil {
		return 0, 0, err
	}

	return int32(v), n, nil
}

func DecodeInt64(buf []byte) (i int64, n int, err error) {
	return DecodeVarint(buf)
}

func DecodeUint32(buf []byte) (i uint32, n int, err error) {
	v, n, err := DecodeUvarint(buf)
	if err != nil {
		return 0, 0, err
	}

	return uint32(v), n, nil
}

func DecodeUint64(buf []byte) (i uint64, n int, err error) {
	return DecodeUvarint(buf)
}
