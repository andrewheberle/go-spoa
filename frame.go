package main

import (
	"bytes"
	"encoding/binary"
	"net"
)

// NewFrame creates a new SPOP frame
func NewFrame(t FrameType, m Metadata, p interface{}) (f Frame) {
	f = Frame{
		Type:     t,
		Metadata: m,
		Payload:  p,
	}
	return f
}

// Marshal method for Frame
func (f Frame) Marshal() ([]byte, error) {
	frame := make([]byte, 0)
	frameType, err := f.Type.Encode()
	if err != nil {
		return nil, err
	}
	frame = append(frame, frameType...)
	metadata, err := f.Metadata.Encode()
	if err != nil {
		return nil, err
	}
	frame = append(frame, metadata...)
	payload, err := Marshal(f.Payload)
	if err != nil {
		return nil, err
	}
	frame = append(frame, payload...)
	return frame, nil
}

// Send method for Frame
func (f Frame) Send(c net.Conn) (n int, err error) {
	// create new buffer
	buf := new(bytes.Buffer)

	// marshal frame
	frame, err := f.Marshal()
	if err != nil {
		return 0, err
	}

	// encode frame size
	err = binary.Write(buf, binary.BigEndian, uint32(len(frame)))
	if err != nil {
		return 0, err
	}

	// write frame size and frame
	return c.Write(append(buf.Bytes(), frame...))
}
