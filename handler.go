package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"log"
	"net"
)

func handleConnection(c net.Conn) {
	var frameLength uint32
	var err error
	var connectionState ConnectionState

	defer c.Close()
	log.Printf("connection from %s", c.RemoteAddr().String())

	for {
		log.Print("New frame...")
		// Get frame length
		err = binary.Read(c, binary.BigEndian, &frameLength)
		if err != nil {
			log.Printf("error: %s", err)
			if err := HandleDisconnect(c, ErrorCodeUnsupportedSize); err != nil {
				log.Printf("disconnect error: %s", err)
			}
			return
		}
		log.Printf("frame length: %d", frameLength)

		// Check frame length
		if frameLength > AgentHello.MaxFrameSize {
			log.Printf("error: %s", ErrorCodeUnsupportedSize)
			if err := HandleDisconnect(c, ErrorCodeUnsupportedSize); err != nil {
				log.Printf("disconnect error: %s", err)
			}
			return
		}

		log.Printf("Current state: %s", connectionState)

		switch connectionState {
		case ConnectionStateConnecting:
			log.Print("Expecting HAPROXY-HELLO")
			// Handle HELLO
			err = HandleHAProxyHello(c, frameLength)
			if err != nil {
				log.Printf("error: %s", err)
				return
			}
			connectionState = ConnectionStateProcessing
		case ConnectionStateProcessing:
			log.Print("Expecting NOTIFY or HAPROXY-DISCONNECT")
			frameType, err := GetFrameType(c)
			if err != nil {
				log.Printf("frame type error: %s", err)
				if err := HandleDisconnect(c, ErrorCodeUnknown); err != nil {
					log.Printf("disconnect error: %s", err)
				}
				return
			}
			log.Printf("Got: %s", frameType)
			switch frameType {
			case FrameTypeNotify:
				if err := HandleNotify(c, frameLength); err != nil {
					log.Printf("notify error: %s", err)
					if err := HandleDisconnect(c, ErrorCodeUnknown); err != nil {
						log.Printf("disconnect error: %s", err)
					}
					return
				}
			case FrameTypeHAProxyDisconnect:
				err = HandleHAProxyDisconnect(c, frameLength)
				if err != nil {
					log.Printf("problem handling HAPROXY-DISCONNECT: %s", err)
				}
				connectionState = ConnectionStateDisconnecting
				c.Close()
			default:
				if err := HandleDisconnect(c, ErrorCodeInvalid); err != nil {
					log.Printf("disconnect error: %s", err)
				}
				return
			}
		case ConnectionStateDisconnecting:
			if err := c.Close(); err != nil {
				return
			}
			if err := HandleDisconnect(c, ErrorCodeNone); err != nil {
				log.Printf("disconnect error: %s", err)
			}
			return
		}
	}
}

func HandleHAProxyHello(c net.Conn, l uint32) error {

	var hapHello PayloadHAProxyHello
	var n, pos int
	var err error
	var buf []byte

	// Read frame
	frame := make([]byte, int(l))
	n, err = c.Read(frame)
	if err != nil {
		log.Printf("error: %s", err)
		return err
	}

	// Check length
	if n != int(l) {
		log.Printf("wrong frame size: %d", n)
		return fmt.Errorf("wrong frame size: %d", n)
	}

	ftype := DecodeFrameType(frame[pos])
	if ftype != FrameTypeHAProxyHello {
		return fmt.Errorf("invalid frame type")
	}

	// Increment pos
	pos++

	// Decode metadata
	metadata, n, err := DecodeMetadata(frame[pos:])
	if err != nil {
		return err
	}

	// HELLO cannot be fragmented
	if !metadata.FrameFlags.Fin {
		return fmt.Errorf("fragmentation unsupported")
	}

	// STREAM-ID and FRAME-ID must be zero
	if metadata.StreamID != 0 || metadata.FrameID != 0 {
		return fmt.Errorf("invalid stream/frame id")
	}

	// Increment pos
	pos = pos + n

	// Grab payload (ie rest of frame)
	payload := frame[pos:]

	err = Unmarshal(payload, &hapHello)
	if err != nil {
		return err
	}

	if err := hapHello.Validate(); err != nil {
		return err
	}

	log.Printf("%v", hapHello)

	agentHelloFrame := NewFrame(FrameTypeAgentHello, Metadata{}, AgentHello)

	log.Printf("Sending AGENT-HELLO: %v", buf)
	_, err = agentHelloFrame.Send(c)
	if err != nil {
		return err
	}

	return nil
}

func HandleHAProxyDisconnect(c net.Conn, l uint32) error {
	var n, pos int
	var err error
	var hapDisconnect PayloadDisconnect

	// reduce l to account for FRAME-TYPE already being read
	l = l - 1

	// Read frame
	frame := make([]byte, int(l))
	n, err = c.Read(frame)
	if err != nil {
		log.Printf("error: %s", err)
		return err
	}

	// Check length
	if n != int(l) {
		log.Printf("wrong frame size: %d", n)
		return fmt.Errorf("wrong frame size: %d", n)
	}

	// Decode metadata
	metadata, n, err := DecodeMetadata(frame[pos:])
	if err != nil {
		return err
	}

	// DISCONNECT cannot be fragmented
	if !metadata.FrameFlags.Fin {
		return fmt.Errorf("fragmentation unsupported")
	}

	// STREAM-ID and FRAME-ID must be zero
	if metadata.StreamID != 0 || metadata.FrameID != 0 {
		return fmt.Errorf("invalid stream/frame id")
	}

	// Increment pos
	pos = pos + n

	// Grab payload (ie rest of frame)
	payload := frame[pos:]

	err = Unmarshal(payload, &hapDisconnect)
	if err != nil {
		return err
	}

	err = HandleDisconnect(c, ErrorCodeNone)
	if err != nil {
		return err
	}

	return nil
}

func HandleNotify(c net.Conn, l uint32) error {
	var n, pos int
	var err error
	
	// reduce l to account for FRAME-TYPE already being read
	l = l - 1

	// Read frame
	frame := make([]byte, int(l))
	n, err = c.Read(frame)
	if err != nil {
		log.Printf("error: %s", err)
		return err
	}

	// Check length
	if n != int(l) {
		log.Printf("wrong frame size: %d", n)
		return fmt.Errorf("wrong frame size: %d", n)
	}

	// Decode metadata
	metadata, n, err := DecodeMetadata(frame[pos:])
	if err != nil {
		return err
	}

	// We don't support fragmentation
	if !metadata.FrameFlags.Fin {
		return fmt.Errorf("fragmentation unsupported")
	}

	log.Printf("NOTIFY frame: %v", frame)
	return nil
}

func HandleDisconnect(c net.Conn, e ErrorCode) error {
	var err error

	frame := make([]byte, 0)
	// FRAME-TYPE
	frame = append(frame, FrameTypeAgentDisconnect.Byte())
	// METADATA
	// FLAGS
	frame = append(frame, []byte{1, 0, 0, 0}...)
	// STREAM-ID & FRAME-ID
	frame = append(frame, []byte{1, 0, 0}...)

	// Build payload
	payload, err := Marshal(PayloadDisconnect{
		StatusCode: e,
		Message:    e.String(),
	})

	if err != nil {
		return err
	}

	frame = append(frame, payload...)
	framelength := make([]byte, 4)
	_ = binary.Write(bytes.NewBuffer(framelength), binary.BigEndian, uint32(len(frame)))
	frame = append(framelength, frame...)

	_, err = c.Write(frame)
	if err != nil {
		log.Printf("write error in disconnect: %s", err)
		return err
	}

	log.Printf("AGENT-DISCONNECT frame = %v", frame)

	return nil
}

func GetFrameType(c net.Conn) (t FrameType, err error) {
	b := make([]byte, 1)

	n, err := c.Read(b)
	if err != nil {
		return FrameTypeUnset, err
	}

	if n != 1 {
		return FrameTypeUnset, fmt.Errorf("invalid size")
	}

	return FrameType(b[0]), nil
}
